package com.binar.chapter3.feature8_2;

import jdk.nashorn.internal.runtime.options.Option;

import java.util.*;
import java.util.stream.*;

public class Topic4 {
    public static void main(String[] args) {
        System.out.println("==== CHAPTER 3 - TOPIC 4 ====");
        // Ini hanya inisialisasi Stream dengan tipe data String
        Stream<String> stream1 = Stream.of("abc");
        Stream<String> stream2 = Stream.empty(); // sama dengan stream 3
        String data = null;
        Stream<String> stream3 = Stream.of(data); // karena null
        Stream<String> stream4 = Stream.of("abc", "def", "ghi");
        Stream<String> stream5 = Arrays.asList("abc", "def", "ghi").stream();

        Stream<Integer> angka1 = Stream.of(10);

//        Student std = new Student();
//        std.setNama("Rizky");
//        std.setNim("007");
//        Stream<Student> student1 = Stream.of(std);

        // Stream tidak bisa pake tipe data primitif
//        Stream<int> test = Stream.of(10);

        List<Integer> angka = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        angka.stream().map(val -> String.valueOf(val)).collect(Collectors.toList()).forEach(v -> {
            System.out.println(v+1);
        });

        Stream<Integer> coba = Stream.of(1, 2, 3).map(v -> v * 2).map(c -> c * 5);
        List<Integer> cobaList = coba.collect(Collectors.toList());

        // List student
        List<Student> students = Arrays.asList(new Student("Rizky", 15, false)
                , new Student("Zidan", 9, true),
                new Student("Agus", 30, true));

        List<String> namaStudent = students.stream().map(student -> student.getNama()).sorted()
                .collect(Collectors.toList());

        Stream<Student> studentStream = students.stream();

        namaStudent.forEach(nama -> System.out.println(nama));

        List<Student> studentsA = new ArrayList<>();
        studentsA.addAll(students);

        List<Student> studentB = Arrays.asList(new Student("Fauzi", 4, false),
                new Student("RMF", 1, true));
        List<List<Student>> studentSekolah = Arrays.asList(studentsA, studentB);
        List<String> namaStudent1Sekolah = studentSekolah.stream().flatMap(kls -> kls.stream())
                .map(std -> std.getNama()).collect(Collectors.toList());
        namaStudent1Sekolah.forEach(nama -> System.out.print(nama + " "));

//        [Student1, Student2, Student3, Student4, Student5]

//        KELAS A                   KELAS B
//        [["Rizky" "Zidan" "Agus"],["Fauzi" "RMF"]] // nama student per kelas

//        Sekolah CERIA
//        ["Rizky" "Zidan" "Agus" "Fauzi" "RMF"] // ini yg udah di flattened

        List<Student> namaStudentBlmBayar = studentSekolah.stream().flatMap(kls -> kls.stream())
                .filter(bayar -> bayar.isSudahBayar() == false)
                .collect(Collectors.toList());
        System.out.println("Siswa yang belum bayar ");
        namaStudentBlmBayar.forEach(student -> {
            System.out.println("Nama : " + student.getNama() + "(" + student.getNim() + ")");
        });

        // Sort
        List<Integer> nilai = Arrays.asList(9, 10, 3, 1, 5, 6); // 34
        // ascending
        System.out.println("Ascending");
        nilai.stream().sorted().forEach(v -> System.out.print(v + ", "));

        // descending
        System.out.println("\nDescending");
        nilai.stream().sorted(Comparator.reverseOrder()).forEach(v -> System.out.print(v + ", "));

        // ascending
        System.out.println("\nAbsensi Student (sort by nim) :");
        studentsA.stream().sorted(Comparator.comparingInt(Student::getNim))
                .forEach(student -> {
                    System.out.println(student.getNim() + " - " + student.getNama());
                });

        System.out.println("\nAbsensi Student (sort by nama) :");
        studentsA.stream().sorted(Comparator.comparing(Student::getNama))
                .forEach(s -> System.out.println(s.getNim() + " - " + s.getNama()));

        // aggregate
        int jumlah = nilai.stream().reduce(0, Integer::sum);
        int jml = 0;
        for (Integer n : nilai) {
            jml+=n;
        }
        System.out.println("jumlah : " + jumlah);

        Optional<Integer> nilaiMax = nilai.stream().max(Comparator.comparingInt(Integer::intValue));
        System.out.println("max : " + nilaiMax.get());

        // Checking
        boolean more5 = nilai.stream().anyMatch(v -> v > 5);
        System.out.println("ada yg lebih dari 5 : " + more5);
        boolean allMore5 = nilai.stream().allMatch(v -> v > 5);
        System.out.println("semua lebih dari 5 : " + allMore5);

        nilai.forEach(nil -> System.out.println(nil * 2));

        // Collector
        Stream<Integer> c = Stream.of(1, 2, 3);
        Double d = c.collect(Collectors.averagingInt(s -> s));
        System.out.println("Rata rata c : " + d);
    }
}
