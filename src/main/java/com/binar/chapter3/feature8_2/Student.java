package com.binar.chapter3.feature8_2;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Student {
    private String nama;
    private Integer nim;
    private boolean sudahBayar;

    public Student(String nama, Integer nim, boolean sudahBayar) {
        this.nama = nama;
        this.nim = nim;
        this.sudahBayar = sudahBayar;
    }
}
