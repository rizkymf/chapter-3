package com.binar.chapter3.junit;

import java.util.Scanner;

public class SimplePrint {

    private String test;

    public String printString(String print) {
        return print;
    }

    public int printInteger(int print) {
        return print;
    }

    public void bagiNol() throws Exception {
        int value = 100/50;
//        System.out.println(value);
        if(value == 2) throw new Exception();
        System.out.println("bagi Nol Nich : " + value);


    }

    public String printStringDua() {
        return "hehe";
    }

    public Double division(Double terbagi, Double pembagi) {
        return terbagi / pembagi;
    }

    public void methodLain() {
        Scanner scan = new Scanner(System.in);
        int inputs = scan.nextInt();
    }

    public void inputs(int inputan) throws Exception {
        switch(inputan) {
            case 1:
                System.out.println("ini satu");
                break;
            default:
                throw new Exception();
        }
    }
}
