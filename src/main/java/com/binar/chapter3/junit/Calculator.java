package com.binar.chapter3.junit;

import com.binar.chapter3.exception.SitiException;

public class Calculator {

    public int add(Integer a, Integer b) throws SitiException {
        if(null == a || null == b) throw new SitiException("Request cannot be null");
        return a + b;
    }

    public int sub(int a, int b) {
//        int a = 10;
//        int b = 5;
        return a - b;
    }

    public int multiply(int a, int b) {
        return a * b;
    }

    public int div(int a, int b) {
        return a / b;
    }
}
