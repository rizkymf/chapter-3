package com.binar.chapter3.exception;

import java.io.IOException;

public class Topic1 {
    public static void main(String[] args) {
//        int data = 50/0;
//        System.out.println("data : " + data);
//        boolean isOkay = true;
//        try {
//            // Exception Arithmetic
//            int data = 50/0;
//            // Exception
//            System.out.println("data : " + data);
//        } catch(ArithmeticException e) {
//            System.out.println("ADA ERROR GAN! because : " + e.getStackTrace()[0]);
//            e.printStackTrace();
////            int data = 50/0;
////            System.out.println(e.getMessage());
//            // jika koneksi gagal
//        } finally {
//            System.out.println("Masih dijalanin kok");
//        }
//        System.out.println("gas terus gan!");
//        try (Scanner scan = new Scanner(System.in)) {
//            System.out.println("Masukkan angka : " + scan.nextInt());
//        } catch(Exception e) {
//            System.out.println("Error because, " + e.getMessage());
//        } finally {
//            System.out.println("Masuk finally nih");
//        }
//        System.out.println("proses selanjutnya . . . ");
//        try {
//            testThrow();
//        } catch (ArithmeticException e) {
//            System.out.println("Ada error nich, gara-gara " + e.getMessage());
//        }

            int a = 10;
            int b = 0;
            int c = Try.division(a, b);
            String z = new String("lal");
            String w = new String("lal");
            if(z == w) {
                System.out.println("ini sama");
            }
//        try {
//            c = 10/0;
//        } catch (ArithmeticException e) {
////            e.printStackTrace();
//            System.out.println("ERROR! perhitungan mean gagal dikarenakan : " + e.getMessage());
//        } catch (SitiException s) {
//            s.printStackTrace();
//        }
    }

    public static void testThrow2() throws ArithmeticException, IOException, IOException {
//        testThrow();
//        coba();
//        coba2();
        siti();
    }

    public static void testThrow() throws ArithmeticException {
        int data = 50/0;
        System.out.println("data : " + data);
    }

    public static void coba() throws IOException {

    }

    public static void coba2() throws IOException{
        int data = 0;
        if(data == 0) throw new IOException("data gabisa 0!");
    }

    public static void siti() throws SitiException {
        int data = 1;
        if(data == 1) throw new SitiException("Siti nanti presentasi");
    }
}
