package com.binar.chapter3.exception;

public class SitiException extends RuntimeException{
    public SitiException(String errorMsg) {
        super("Request cannot be null");
    }
}
