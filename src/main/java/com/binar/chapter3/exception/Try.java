package com.binar.chapter3.exception;

import java.util.Date;

public class Try {

    public Date parseDate(String date) {
//        Date pDate = new Date().;
        return new Date();
    }

    public static int division(int a, int b) {
        int div = 0;
        try{
            div =  a / b;
        } catch(ArithmeticException e) {
            System.out.println("ERROR pembagian 0");
        }
        return div;
    }
}
