package com.binar.chapter3;

import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

@FunctionalInterface
public interface FunctionalCalculation {
    OptionalDouble calculate(List<Integer> nilai);
}
