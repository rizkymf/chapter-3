package com.binar.chapter3.algorithm3;

import javax.swing.text.html.Option;
import java.util.*;
import java.util.stream.Collectors;

public class Algorithm {
    public static void main(String[] args) {

    }

    boolean isAnagram(String kata1, String kata2) {
        if(kata1.length() != kata2.length()) return false;
        String[] kata11 = kata1.split("");
        String[] kata22 = kata2.split("");
        boolean anagram = true;
        for(int i = 0; i < kata11.length; i++) {
            if(kata11[i].equals(kata22[i])) anagram = false;
        }
        return anagram;
    }

    String phoneBook(String searchName) {
        Map<String, String> phone = new HashMap<>();
        phone.put("Beben", "081777777777");
        phone.put("Baban", "081877777777");
        phone.put("Bobon", "087877777777");
        Optional<Map<String, String>> t = Optional.of(phone);
        if(null != phone.get(searchName)) {
            return searchName + " = " + phone.get(searchName);
        } else {
            return "Nama nggak ditemukan";
        }
    }
}
