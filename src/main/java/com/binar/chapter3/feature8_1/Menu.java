package com.binar.chapter3.feature8_1;

@FunctionalInterface
public interface Menu {
    void showMenu();
}
