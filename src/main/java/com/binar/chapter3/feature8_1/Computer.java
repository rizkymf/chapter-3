package com.binar.chapter3.feature8_1;

import com.sun.istack.internal.Nullable;
import jdk.nashorn.internal.runtime.options.Option;
import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Setter
@Getter
public class Computer {

    private Optional<Soundcard> soundcard;
}
