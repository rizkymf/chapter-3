package com.binar.chapter3.feature8_1;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Topic3 {

//    static Optional<Computer> computer;

    public static void main(String[] args) {
        System.out.println("==== CHAPTER 3 - TOPIC 3 ====");
        // Optional
        Computer computer = new Computer();
        Optional<Computer> comp = Optional.of(computer);
//        System.out.println(comp.get());
//        Soundcard soundcard = new Soundcard();
//        Optional<Soundcard> sc = Optional.of(soundcard);
//        USB usb = new USB();
//        Optional<USB> usb1 = Optional.of(usb);
//        System.out.println(usb1.get().getVersion());
//        System.out.println(comp.get().getSoundcard().get().getUsb().get().getVersion());
//        System.out.println(comp.flatMap(Computer::getSoundcard)
//                .flatMap(soundcard1 -> soundcard1.getUsb()));
//        String res = comp.flatMap(Computer::getSoundcard)
//                .flatMap(Soundcard::getUsb).map(USB::getVersion).orElse("UNKNOWN");
//        System.out.println(res);
//        System.out.println(comp.flatMap(Computer::getSoundcard)
//                .flatMap(Soundcard::getUsb).map(USB::getVersion).orElse("UNKNOWN"));
        System.out.println("-----------------------------------------------------");
        // Lambda
        Runnable r = new Runnable() {
            @Override
            public void run() {
                System.out.println("I'm preparing 1");
                System.out.println("I'm running! 1");
            }
        };
        r.run();

        Runnable r2 = () -> {
            System.out.println("I'm preparing 2");
            System.out.println("I'm running! 2");
        };
        r2.run();

        SatuMethod satuMethod = () -> {
            sayHi();
        };
        satuMethod.sayHi();

//        DuaMethod duaMethod = () -> {
//            System.out.println("Hellooooo.....");
//        };
//        duaMethod.method1();

        withParam withParam = (t) -> System.out.println(t);
        withParam.methodParam("HAAAAY");

        withReturn withReturn = (a, b) -> a+b;
        int hasil = withReturn.add(5, 10);
        System.out.println("Hasilnya : " + hasil);

        Consumer<String> consumer = v -> System.out.println(v);
        consumer.accept("Ini Consumer");

        BiConsumer<String, String> biconsumer = (v, w) -> System.out.println(v + w);
        biconsumer.accept("Hallo ", "Rizky");

        Supplier<String> supplierWithReturn = () -> {
            return "Aku ganteng";
        };
        Supplier<String> supplier = () -> "Aku ganteng 2";
        System.out.println(supplier.get());

        ManyParam manyParam = (a, b, c, d, s) -> System.out.println(s + " " + (a+b+c+d));
        manyParam.soMany(1, 2, 3, 4, "Hasilnya :");

        LambaMethod lambaMethod = new LambaMethod();
        Contoh contoh = new Contoh();
        lambaMethod.methodLamba("Hallo aku method Lambda, mau ngejalanin method nich",
                () -> contoh.sayHi());
        System.out.println("-----------------------------------------------------");
        // Method reference
        // static
        ReferenceInterface referenceInterface0 = () -> Reference.staticMethod(); // pake lambda
        ReferenceInterface referenceInterface1 = Reference::staticMethod; // pake method reference
        referenceInterface1.execute();

        // non static
        Reference reference = new Reference();
        ReferenceInterface referenceInterface2 = reference::nonStaticMethod;
        referenceInterface2.execute();

        // constructor
        ReferenceInterface referenceInterface3 = Reference::new;
        referenceInterface3.execute();

        System.out.println("-----------------------------------------------------");
        // Lambda di collection
        List<String> kelas = Arrays.asList("kelas A", "kelas B");
        kelas.forEach(val -> System.out.println(val));
        kelas.forEach(System.out::println);

        Map<String, Integer> nilai = new HashMap<>();
        nilai.put("Rizky", 10);
        nilai.put("Rian", 10);
        nilai.put("Siti", 100);
        nilai.forEach((k, v) -> System.out.println("key " + k + " value " + v));

        System.out.println("-----------------------------------------------------");
        Scanner scan = new Scanner(System.in);
        Menu menu = () -> {
            System.out.println("=== MAIN MENU ===\n" +
                    "1. cari mean median dan modus\n" +
                    "2. cari frekuensi nilai semua kelas\n" +
                    "0. Exit");
        };
        menu.showMenu();
        try {
            int input = scan.nextInt();
            if(input < 0 && input > 2) throw new Exception();

            switch(input) {
                case 0:
                    break;
            }
        } catch(Exception e) {
            System.out.println("ERROR pilihan tidak dikenali!");
        }
    }

    static void sayHi() {
        System.out.println("Hiiiiii");
    }
}
