package com.binar.chapter3.feature8_1;

public interface DuaMethod {
    void method1();
    void method2();

    // challenge 2;
    void kelompokData();
    void meanMedianModus();

    // kalkulasi;
    void mean();
    void median();
    void modus();

    //
    void readFile();
    void writeFile();
}
