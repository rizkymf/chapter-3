package com.binar.chapter3.feature8_1;

public class LambaMethod {

    public void methodLamba(String intro, SatuMethod satuMethod) {
        System.out.println(intro);
        satuMethod.sayHi();

        SatuMethod sm = new SatuMethod() {
            @Override
            public void sayHi() {
                System.out.println("cara tradisional");
            }
        };
        sm.sayHi();

        SatuMethod sm1 = () -> System.out.println("implementasi cara lambda");
        SatuMethod sm2 = () -> System.out.println("Implementasi cara lambda 2");
        sm1.sayHi();
        sm2.sayHi();
    }
}
