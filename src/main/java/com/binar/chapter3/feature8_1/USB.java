package com.binar.chapter3.feature8_1;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class USB {
    public String getVersion() {
        return "3.0.0";
    }
}
