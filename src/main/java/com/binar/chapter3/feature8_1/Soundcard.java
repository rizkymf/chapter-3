package com.binar.chapter3.feature8_1;

import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Setter
@Getter
public class Soundcard {
    private Optional<USB> usb;
}
