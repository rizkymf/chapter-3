package com.binar.chapter3.feature8_1;

public class Reference {
    Reference() {
        System.out.println("ini constructor");
    }

    public static void staticMethod() {
        System.out.println("ini static method");
    }

    public void nonStaticMethod() {
        System.out.println("ini instance method");
    }
}
