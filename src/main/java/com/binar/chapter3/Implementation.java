package com.binar.chapter3;

import java.sql.SQLOutput;
import java.util.*;
import java.util.stream.Collectors;

public class Implementation {
    public static void main(String[] args) {
        countLetter("keramas").forEach((k,v) -> {
            System.out.print(k + ":" + v + " ");
        });
        System.out.println("-------------------------");
        distinctNumber("2,2,2,8,8,8,8,8,4,4,4,4,10,2,2,2,2,6,6,6,6,6,2,2,2,2,2,2");
    }

    public static Double mean(List<Integer> nilai) {
        FunctionalCalculation calculation = (list) -> list.stream().mapToDouble(d -> d).average();;
        return calculation.calculate(nilai).getAsDouble();
    }

    public static Map<String, Integer> countLetter(String s) {
        List<String> stringList = Arrays.asList(s.split(""));
        Map<String, Integer> res = stringList.stream()
                .collect(Collectors.toMap(k -> String.valueOf(k), v -> 1, Integer::sum));
        return res;
    }

    public static void distinctNumber(String numbers) {
        List<String> stringList = Arrays.asList(numbers.split(","));
        stringList.stream().distinct().forEach(v -> System.out.print(v + ","));
        Set<String> number = stringList.stream().collect(Collectors.toSet());
//        number.forEach(num -> System.out.print(num + ","));
    }

}
