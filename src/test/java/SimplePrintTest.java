import com.binar.chapter3.junit.SimplePrint;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SimplePrintTest {

    SimplePrint sp = new SimplePrint();

    @Test
    @DisplayName("Param kosong")
    void printDua() {
        Assertions.assertEquals("hehe", sp.printStringDua());
    }

    @Test
    @DisplayName("Positive Test - SimplePrint String success")
    void printStringSuccess() {
//        SimplePrint sp = new SimplePrint();
        String test = "Hello World";
        Assertions.assertEquals(test, sp.printString(test));
    }

    @Test
    @DisplayName("Positive Test - print integer success")
    void printIntegerSuccess() {
//        SimplePrint sp = new SimplePrint();
        int test = 10;
        Assertions.assertEquals(test, sp.printInteger(test));
    }

    @Test
    @DisplayName("TEST AJA")
    void outputError() throws Exception {
//        SimplePrint sp = new SimplePrint();
        sp.bagiNol();
    }

    @Test
    @DisplayName("Positive Test Case --- division")
    void testPembagi() {
        Double terbagi = 100d;
        Double pembagi = 50d;
        Assertions.assertEquals(2, sp.division(terbagi, pembagi));
    }

    @Test
    @DisplayName("Negative Test Case --- division")
    void testPembagiNeg() {
        Double terbagi = 100d;
        Double pembagi = 0d;
        Assertions.assertThrows(ArithmeticException.class, () -> sp.division(terbagi, pembagi));
    }

    // username : null
    // password : null
}
