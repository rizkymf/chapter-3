import com.binar.chapter3.exception.SitiException;
import com.binar.chapter3.junit.Calculator;
import org.junit.jupiter.api.*;

public class CalcTest {

    private int value;

    @BeforeAll
    void testBefore() {
        Calculator calculator = new Calculator();
    }

    @Test
    @DisplayName("Do Nothing")
    void testNothing() {

    }

    @Test
    @DisplayName("POSITIVE TEST -- Calculator.add()")
    void testAddSuccess() {
        Calculator calculator = new Calculator();
        Assertions.assertEquals(20, calculator.add(10, 10));
    }

    @Test
    @DisplayName("NEGATIVE TEST -- Calculator.add()")
    void testAddFailed() {
        Calculator calculator = new Calculator();
        Exception e = Assertions.assertThrows(SitiException.class, () -> calculator.add(null, null));
//        Exception e = Assertions.assertThrows(SitiException.class, calculator::add);
        Assertions.assertEquals("Request cannot be null", e.getMessage());
        value+=200;
    }

    @Test
    @DisplayName("POSITIVE TEST -- Sub")
    void testSubSuccess() {
        Calculator calculator = new Calculator();
        Assertions.assertEquals(5, calculator.sub(10, 5));
    }

    @Test
    @DisplayName("NEGATIVE TEST -- Sub")
    void testSubFailedNull() {
        Calculator calculator = new Calculator();
//        Exception e = Assertions.assertThrows();
    }

    void readFileTest() {
        // file kosong
        // nilai ga valid; 10, 9, B

    }

    @Test
    @DisplayName("Luas Balok")
    void testLuasBalok() {
        int a = 10;
        int b = 50;
//        Assertions.assertEquals(500, panggilMethod);
    }

    @Test
    @DisplayName("Test Mean")
    void testMean() {

    }

    @Test
    @DisplayName("Test Median")
    void testMedian() {

    }

    @Test
    @DisplayName("Test Modus")
    void testModus() {

    }

}
